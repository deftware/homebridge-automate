const schedule = require('node-schedule');
let Service, Characteristic;

module.exports = function (homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory("homebridge-automate", "HomeAutomate", homebridgeAutomate);
};

homebridgeAutomate.prototype.getServices = function () {
    this.informationService = new Service.AccessoryInformation();
    this.informationService
        .setCharacteristic(Characteristic.Manufacturer, "Deftware")
        .setCharacteristic(Characteristic.Model, "MotionSensor v1")
        .setCharacteristic(Characteristic.SerialNumber, this.uuid);

    this.motionService = new Service.MotionSensor(this.name + ' Trigger');
    this.motionService
        .getCharacteristic(Characteristic.MotionDetected)
        .on('get', this.getMotion.bind(this));

    return [this.informationService, this.motionService];
}

homebridgeAutomate.prototype.getMotion = function (callback) {
    callback(null, this.motionTriggered);
}

function homebridgeAutomate(log, config, api) {
    let UUIDGen = api.hap.uuid;
    this.log = log;
    this.intervals = config['intervals'];
    this.scheduledJobs = {};
    this.name = config['name'];
    this.uuid = UUIDGen.generate(this.name);
    this.motionTriggered = false;

    // Set up intervalls
    for (let interval of this.intervals) {
        this.log(`Setting up interval ${interval} for sensor ${this.name}`);
        this.scheduledJobs[interval] = schedule.scheduleJob(interval, () => {
            this.motionTriggered = true;
            this.motionService.getCharacteristic(Characteristic.MotionDetected).updateValue(true);
            this.log(`Triggering ${this.name} for interval ${interval}`);
            setTimeout(function () {
                this.motionService.getCharacteristic(Characteristic.MotionDetected).updateValue(false);
                this.motionTriggered = false;
            }.bind(this), 3000);
        });
    }
}
