# Homebridge Automate

A simple way to have cronjob style intervals trigger actions in your home.

# Install/Usage

1. Install with `sudo npm install -g homebridge-automate`
2. Add new dummy motion sensors with cronjob intervals to your `accessories` array:

```json
{
    "accessory": "HomeAutomate",
    "name": "<Unique name>",
    "intervals": [
        "<your intervals here>"
    ]
}
```

You can now trigger actions based on this motion sensor which triggers at your set intervals.
For help with finding crontab intervals, check out [crontab guru](https://crontab.guru/)
